# HITRAN Database Files

The files come from https://www.cfa.harvard.edu/hitran/ ; see Rothman et al 2012, https://www.cfa.harvard.edu/hitran/Download/HITRAN2012.pdf

This is a convenience repo to avoid having certain other code repos become bloated by input files. 

